//
// Created by white on 16.05.16.
//

#ifndef NASTYA_ACTOR_H
#define NASTYA_ACTOR_H
#include <string>
#include <iostream>
#include <fstream>
class Actor {
private:
    std::string name;
    int id;
    bool loadFromFile(std::string path);
public:
    int getId();
    std::string getName();
    Actor(std::string);
};


#endif //NASTYA_ACTOR_H
