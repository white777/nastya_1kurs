//
// Created by white on 16.05.16.
//

#include "Actor.h"
using namespace std;

int Actor::getId() { return id; }
string Actor::getName() {return name; }
bool Actor::loadFromFile(std::string filename) {
    ifstream f(filename.c_str());
    if (f.is_open()==false){
        return false;
    }
    string id_s;
    getline(f, id_s);
    getline(f, name);
    id = stoi(id_s);
    return true;

}
Actor::Actor(std::string path) {
    if(!loadFromFile(path)) cout << "Unable to load: " << path;
}