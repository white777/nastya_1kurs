#include "Seans.h"
using namespace std;

    bool Seans::loadFromFile(string filename){ //загружаем поле из файла
        ifstream f(filename);
        if (f.is_open()==false){
            return false;
        }
        string movie_s, id_s, zones_s;
        getline(f, id_s);
        movie = stoi(id_s);

        getline(f, movie_s); //1 строчка- название фильма
        movie = stoi(movie_s);
        getline(f, date); //2 строчка- дата
        getline(f, time); //3 строчка- время
        getline(f, zones_s); //4 строчка- кол-во ценовых зон

        int zones  =stoi(zones_s);

        for(int j=0; j<zones; j++){
            string z;
            getline(f, z);
            zonePrices.push_back(stoi(z));
        }
        string line;

        while (getline(f, line)){ //Заполняем места
            vector<int> row;
            for (int i=0; i<line.length(); i++){
                row.push_back( line[i]-'0' );  //convert char to int
            }

            field.push_back(row);
        }
        f.close();
        return true;
    }
    bool Seans::isNotOrdered(int col, int row){ //проверка места на занятость
        if (field[col][row]==Status::TAKEN){
            return true;
        }
        return false;
    }
    bool Seans::orderPlace(int col, int row){

        if (col<0 or col>field.size()-1 or row<0 or row>field[col].size()-1){
            //проверяем, входил ли место в наше поле мест
            cout <<  "Not in field"  << "(" << col << ","<< row << ")" << endl;
            return false;
        }
        if (field[col][row]==Status::NOSEAT){
            //такого кресла нет
            cout <<  "There is no seat here" << "(" << col << ","<< row << ")"  << endl;
            return false;
        }
        if (field[col][row]!=Status::TAKEN){ //проверяем, не занято ли место
            cout << "Your place " << "(" << col << ","<< row << ")" <<" is ordered for price: " << getPrice(field[col][row]) << endl;
            field[col][row]=Status::TAKEN;
        } else {
            cout <<  "The place is taken "  << "(" << col << ","<< row << ")" << endl;
            return false;
        } //место занято
        return true;
    }
//    void Seans::printField(){ //отрисуем места
//        cout << "------------------------"<< endl;
////        cout << name << endl;
//        for (int i=0; i<field.size(); i++){
//            for (int j=0; j<field[i].size(); j++){
//                cout << field[i][j];
//            }
//            cout << endl;
//        }
//        cout << "------------------------"<< endl;
//
//    }
    bool Seans::saveToFile(string filename){
        ofstream f;
        f.open(filename);
        if (!f.is_open()){
            return false; //не открыть файл для записи
        }
        f<<movie<<endl;
        for (int i=0; i<field.size(); i++){
            for (int j=0; j<field[i].size(); j++){
                f << field[i][j];
            }
            f << endl;
        }
        return true;
    }
Seans::Seans(string filename){ //конструктор класса
        if (!loadFromFile(filename)){
            //Ошибка агрузки файла
            cout << "Error loading file";
            exit(0);
        }
    }
int Seans::getId() {return id; }
int Seans::getPrice(int zone) {
    return zonePrices[zone-1];
}