//
// Created by white on 15.05.16.
//

#ifndef NASTYA_MOVIE_H
#define NASTYA_MOVIE_H
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include "help.h"

class Movie {
private:
    string name;
    int length; //in minutes
    int director; //surname of director
    vector<int> actors;
    int from_age;
    int year;
    int id;
    bool loadFromFile(string filename);
public:
    Movie(string filename);
    int getDirector();
    int getFromAge();
    int getLength();
    string getName();
    int getYear();
    int getId();
    vector<int> getActors();
    //void print();
};

#endif //NASTYA_MOVIE_H
