//
// Created by white on 15.05.16.
//

#include "Theatre.h"


//vector<Seans> Theatre::getSeansesWithName(string name) {
//    vector<Seans> result;
//    for (int i=0; i<seanses.size(); i++){
//        if (seanses[i].name==name){
//            result.push_back(seanses[i]);
//        }
//    }
//    return result;
//}

//vector<Seans> Theatre::getSeansesWithDate(string date) {
//    vector<Seans> result;
//    for (int i=0; i<seanses.size(); i++){
//        if (seanses[i].date==date){
//            result.push_back(seanses[i]);
//        }
//    }
//    return result;
//}
Theatre::Theatre(vector<string> seanses_files,
                 vector<string> directors_files,
                 vector<string> movies_files,
                 vector<string> actors_files) {
    loadSeanses(seanses_files);
    loadDirectors(directors_files);
    loadMovies(movies_files);
    loadActors(actors_files);

}
bool Theatre::loadSeanses(vector<string> files){
    for (int i=0; i<files.size(); i++){
        Seans s(files[i]);
        seanses.push_back(s);
    }
}
bool Theatre::loadActors(vector<string> files){
    for (int i=0; i<files.size(); i++){
        Actor s(files[i]);
        actors.push_back(s);
    }
}
bool Theatre::loadMovies(vector<string> files){
    for (int i=0; i<files.size(); i++){
        Movie s(files[i]);
        movies.push_back(s);
    }
}
bool Theatre::loadDirectors(vector<string> files){
    for (int i=0; i<files.size(); i++){
        Director s(files[i]);
        directors.push_back(s);
    }
}

void Theatre::printSeanses(){
    for (int i=0; i<seanses.size(); i++){
        printSeans(i);
//        seanses[i].printField();
    }
}
void Theatre::printMovie(int id) {
    Movie m = getMovieById(id);

            cout << "Movie title: " << m.getName() << endl;
            cout << "Movie length: " << m.getLength()<<" minutes" << endl;
            cout << "Movie director: " << getDirectorById(m.getDirector()).getName() << endl;
            cout << "Movie from_age: " << m.getFromAge()<< endl;
            cout << "Movie year: " << m.getYear() << endl;
            cout << "Movie actors: ";
            for (int i=0; i<m.getActors().size(); i++){
                cout << getActorById(m.getActors()[i]).getName();
                if (i!=m.getActors().size()-1) cout << ", ";

            }
            cout << endl;
            cout << "-" << endl;



}
void Theatre::printMovies(){
    for (int i=0; i<movies.size(); i++){
        printMovie(movies[i].getId());
//        movies[i].print();
    }
}
void Theatre::printDirectors(){
    for (int i=0; i<directors.size(); i++){
        directors[i].print();
    }
}
void Theatre::printActors(){
    for (int i=0; i<actors.size(); i++){
        cout << actors[i].getName() << endl;
    }
}
void Theatre::printActor(int id) {
    cout << actors[id].getName() << endl;
}

void Theatre::printSeans(int id) {//отрисуем места
        cout << "------------------------"<< endl;
        //cout << movies[seanses[id].movie].getName() << endl;
    cout << getMovieById(seanses[id].movie).getName() << endl;
        for (int i=0; i<seanses[id].field.size(); i++){
            for (int j=0; j<seanses[id].field[i].size(); j++){
                cout << seanses[id].field[i][j];
            }
            cout << endl;
        }
        cout << "------------------------"<< endl;


}
void Theatre::print(){
    printMovies();
    printSeanses();
    //printDirectors();
}

Movie Theatre::getMovieById(int id) {
    for(int i=0; i<movies.size(); i++){
        if (movies[i].getId() == id){
            return movies[i];
        }
    }
}
Director Theatre::getDirectorById(int id){
    for(int i=0; i<directors.size(); i++){
        if (directors[i].getId() == id){
            return directors[i];
        }
    }
}
Actor Theatre::getActorById(int id) {
    for(int i=0; i<actors.size(); i++){
        if (actors[i].getId() == id){
            return actors[i];
        }
    }
}
vector<Seans> Theatre::getSeanses() {return seanses; }