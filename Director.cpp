//
// Created by white on 15.05.16.
//

#include "Director.h"
using namespace std;

int Director::getId(){return id;}
string Director::getName(){return name;}
bool Director::loadFromFile(std::string filename) {
    ifstream f(filename.c_str());
    if (f.is_open()==false){
        return false;
    }
    string id_s;
    getline(f, id_s);
    getline(f, name);

    id = stoi(id_s);
    return true;
}
Director::Director(string path) {
    if(!loadFromFile(path)) cout << "Cant load director: " << path << endl;
}
void Director::print() {
    cout << "[" <<id << "]" << " Director name: " << name << endl;
}

