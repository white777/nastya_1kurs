//
// Created by white on 15.05.16.
//

#include "Movie.h"
using namespace std;
int Movie::getDirector(){
    return director;
}
int Movie::getFromAge(){
    return from_age;
}
int Movie::getYear(){
    return year;
}
int Movie::getLength(){
    return length;
}

string Movie::getName(){
    return name;
}

bool Movie::loadFromFile(string filename){
    ifstream f(filename.c_str());
    if (f.is_open()==false){
        return false;
    }
    string id_s, year_s, length_s, director_s, from_age_s;
    getline(f, id_s);
    getline(f, name);
    getline(f, year_s);
    getline(f, length_s);
    year = stoi(year_s);
    length = stoi(length_s);

    //get actors
    string line;
    getline(f, line);
    actors = splitStringToInt(line, ',');

    getline(f, director_s);
    getline(f, from_age_s);
    director = stoi(director_s);
    from_age = stoi(from_age_s);
    id = stoi(id_s);

    f.close();
    return true;

}
Movie::Movie(string filename){
    if(!loadFromFile(filename)){
        cout << "Cant create movie: " << filename << endl;
    }
}
int Movie::getId() {return id; }
vector<int> Movie::getActors() {return actors; }