#include "help.h"

using namespace std;
bool hasEnding (std::string const &fullString, std::string const &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

vector<string> getTxtFilesInPath(string path){

    DIR *dir;
    vector<string> r;
    struct dirent *ent;
    if ((dir = opendir (path.c_str())) != NULL) {
        /* print all the files and directories within directory */
        while ((ent = readdir (dir)) != NULL) {
            if (hasEnding(string(ent->d_name), ".txt")) {
                r.push_back(path+string(ent->d_name));
            }

        }
        closedir (dir);
    }
    return r;
}

vector<int> splitStringToInt(string str, char separator){

    std::vector<int> vect;

    std::stringstream ss(str);

    int i;

    while (ss >> i)
    {
        vect.push_back(i);

        if (ss.peek() == separator)
            ss.ignore();
    }

    return vect;
}