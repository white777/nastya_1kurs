//
// Created by white on 15.05.16.
//

#ifndef NASTYA_STATUSES_H
#define NASTYA_STATUSES_H

class Status{
public:
    static const int NOSEAT=0;
    static const int TAKEN=1;

    static const int ZONE1=2;
    static const int ZONE2=3;
    static const int ZONE3=4;
};
#endif //NASTYA_STATUSES_H
