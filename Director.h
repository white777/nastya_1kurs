//
// Created by white on 15.05.16.
//

#ifndef NASTYA_DIRECTOR_H
#define NASTYA_DIRECTOR_H
#include <string>
#include <iostream>
#include <fstream>

class Director {
private:
    int id;
    std::string name;
    bool loadFromFile(std::string path);
public:
    int getId();
    std::string getName();
    Director(std::string path);
    void print();

};


#endif //NASTYA_DIRECTOR_H
