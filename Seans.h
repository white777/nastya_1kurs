#include <vector>
#include <fstream>
#include <string>
#include <iostream>
#include <dirent.h>
#include "Statuses.h"

using namespace std;
class Seans{ //класс сеанса
private:
    int id;
    vector<int> zonePrices;
public:
    vector<vector<int> > field; //места
    //представляет собой двумерный ПРЯМОУГОЛЬНЫЙ массив статуса занятости кресла .
    // 0 -кресла нет
    // 1- место свободно.
    // 2- занято


    int movie;
    string time;
    string date;


    bool loadFromFile(string filename); //загружаем поле из файла

    bool isNotOrdered(int col, int row); //проверка места на занятость

    bool orderPlace(int col, int row); // забронитьвать место
    void printField();//отрисуем места

    bool saveToFile(string filename); //сохранить в файл
    Seans(string filename); //конструктор
    int getId();
    vector<int> getZonePrices();
    int getPrice(int zone);
};

