//
// Created by white on 11.05.16.
//

#ifndef NASTYA_HELP_H
#define NASTYA_HELP_H

#endif //NASTYA_HELP_H

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <dirent.h>

using namespace std;
bool hasEnding (std::string const &fullString, std::string const &ending);

vector<string> getTxtFilesInPath(string path);
vector<int> splitStringToInt(string str, char separator);
