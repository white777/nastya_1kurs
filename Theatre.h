//
// Created by white on 15.05.16.
//

#ifndef NASTYA_THEATRE_H
#define NASTYA_THEATRE_H
#include "Seans.h"
#include "Movie.h"
#include "Director.h"
#include "Actor.h"

class Theatre{
private:
    bool loadSeanses(vector<string>);
    bool loadDirectors(vector<string>);
    bool loadMovies(vector<string>);
    bool loadActors(vector<string>);
    vector<Seans> seanses;
    vector<Director> directors;
    vector<Movie> movies;
    vector<Actor> actors;
public:
    Theatre(vector<string>, vector<string>, vector<string>, vector<string>);
    //vector<Seans> getSeansesWithName(string);
    //vector<Seans> getSeansesWithDate(string);


    //get
    Movie getMovieById(int id);
    Director getDirectorById(int id);
    Actor getActorById(int id);
    vector<Seans> getSeanses();

    //print
    void printActor(int);
    void printActors();
    void printSeans(int);
    void printSeanses();
    void printMovie(int);
    void printMovies();
    void printDirectors();
    void print();

};



#endif //NASTYA_THEATRE_H
